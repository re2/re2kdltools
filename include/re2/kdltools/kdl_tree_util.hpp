/*
 * kdl_tree_util.hpp
 *
 *  Created on: Mar 4, 2013
 *      Author: andrew.somerville
 */

#pragma once

#include <kdl/tree.hpp>
#include <kdl/treefksolverpos_recursive.hpp>
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <vector>
#include <urdf/model.h>

typedef std::vector<KDL::SegmentMap::const_iterator> KdlElementChildren;
typedef boost::shared_ptr<KDL::Tree>       KdlTreePtr;
typedef boost::shared_ptr<const KDL::Tree> KdlTreeConstPtr;

typedef boost::shared_ptr<KDL::TreeFkSolverPos_recursive> KdlTreeFkSolverPtr;
typedef boost::shared_ptr<const KDL::TreeFkSolverPos_recursive> KdlTreeFkSolverConstPtr;


Eigen::Vector4d  aggrigatePointMasses( const Eigen::Vector4d & pm0, const Eigen::Vector4d pm1 );
void printTree( const KDL::Tree & tree );
double calcTreeMass( const KDL::Tree & tree );

Eigen::Affine3d
getTransform( KDL::TreeFkSolverPos_recursive & treeSolver,
              const std::string & baseFrame,
              const std::string & tipFrame,
              const Eigen::VectorXd & jointPositions );

void transformEigenToKdl( const Eigen::Affine3d &e, KDL::Frame &k);
void transformEigenToKdl( const Eigen::Matrix4d &e, KDL::Frame &k);


bool kdlRootInertiaWorkaround(const urdf::Model& robot_model, KDL::Tree& tree);
bool kdlRootInertiaWorkaround(const std::string& xml, KDL::Tree& tree);
bool kdlRootInertiaWorkaround(TiXmlDocument *xml_doc, KDL::Tree& tree);


const KDL::TreeElement & getTreeElement( const KDL::Tree & tree, const std::string & segmentName );
