/*
 * COPYRIGHT (C) 2005-2012
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2012 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */
#include <kdl_parser/kdl_parser.hpp>
#include <kdl/jntarray.hpp>
#include <urdf_model/model.h>
#include <boost/foreach.hpp>

#include <string>

namespace re2
{
namespace kdltools
{

typedef std::map<std::string,double> JointNameToAngleMap;

// Convenience function to get get chain and do error checking in one step
boost::shared_ptr<KDL::Chain>
buildChain( const std::string & modelUrdf,
            const std::string & rootName,
            const std::string & tipName   );

// Get limits from urdf for chain specified by root and tip
void
getJointLimits( const std::string & modelUrdf,
                const std::string & rootName,
                const std::string & tipName,
                KDL::JntArray &     jointMin,
                KDL::JntArray &     jointMax  );

void
getJointLimits( const urdf::ModelInterface & robotModel,
                const std::string & rootName,
                const std::string & tipName,
                KDL::JntArray &     jointMin,
                KDL::JntArray &     jointMax  );

void
getJointLimits( const urdf::ModelInterface &   robotModel,
                JointNameToAngleMap & jointMins,
                JointNameToAngleMap & jointMaxs  );


void
getJointLimits( const boost::shared_ptr<const urdf::Link> & root,
                JointNameToAngleMap & jointMins,
                JointNameToAngleMap & jointMaxs  );


}
}
