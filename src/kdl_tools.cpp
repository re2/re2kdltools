/*
 * COPYRIGHT (C) 2005-2012
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2012 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <re2/kdltools/kdl_tools.h>

#include <kdl_parser/kdl_parser.hpp>
#include <kdl/jntarray.hpp>
#include <urdf/model.h>
#include <boost/foreach.hpp>

#include <ros/ros.h>

#include <string>

namespace re2
{
namespace kdltools
{

boost::shared_ptr<KDL::Chain> buildChain( const std::string & modelUrdf,
                                          const std::string & rootName,
                                          const std::string & tipName   )
{
    KDL::Tree   tree;
    boost::shared_ptr<KDL::Chain>  chain( new KDL::Chain() );

    if (!kdl_parser::treeFromString(modelUrdf, tree))
    {
        ROS_ERROR("Could not initialize tree object");
        throw std::string( "fail to load tree from xml" );
    }

    if (!tree.getChain(rootName, tipName, *chain))
    {
        std::stringstream errorStream;
        errorStream << "fail to load chain starting at "<< rootName << " ending with " << tipName << " from xml\n";

        if( tree.getSegment(rootName) == tree.getSegments().end() )
            errorStream << "[" << rootName << "] segment not found.\n";

        if( tree.getSegment(tipName) == tree.getSegments().end() )
            errorStream << "[" << tipName << "] segment not found.\n";

        errorStream << "segmentMap looks like: ";
        const KDL::SegmentMap & segmentMap = tree.getSegments();
        BOOST_FOREACH( KDL::SegmentMap::value_type entry, segmentMap )
        {
            errorStream << entry.second.segment.getName() << " > ";
        }

        ROS_ERROR("Could not initialize chain object: %s", errorStream.str().c_str() );
        throw errorStream.str();
    }

    return chain;
}

void getJointLimits( const std::string & modelUrdf,
                     const std::string & rootName,
                     const std::string & tipName,
                     KDL::JntArray &     jointMin,
                     KDL::JntArray &     jointMax  )
{
    urdf::Model robotModel;
    if (!robotModel.initString(modelUrdf))
    {
        ROS_FATAL("Could not initialize robot model");
        throw std::string( "Could not initialize robot model" );
    }

    getJointLimits( robotModel, rootName, tipName, jointMin, jointMax );
}

void getJointLimits( const urdf::ModelInterface & robotModel,
                     const std::string & rootName,
                     const std::string & tipName,
                     KDL::JntArray &     jointMin,
                     KDL::JntArray &     jointMax  )
{
    int numJoints = 0;
    // get joint maxs and mins
    boost::shared_ptr<const urdf::Link>  link = robotModel.getLink( tipName );
    boost::shared_ptr<const urdf::Joint> joint;

    while (link && link->name != rootName )
    {
        joint = robotModel.getJoint( link->parent_joint->name );
        if (!joint)
        {
            std::stringstream errorString;
            errorString << "Could not find joint: "<< link->parent_joint->name;
            ROS_ERROR( "%s", errorString.str().c_str() );
            throw errorString.str();
        }
        if (joint->type != urdf::Joint::UNKNOWN && joint->type != urdf::Joint::FIXED)
        {
            ROS_DEBUG( "adding joint: [%s]", joint->name.c_str() );
            numJoints++;

        }
        link = robotModel.getLink( link->getParent()->name );
    }

    jointMin.resize(numJoints);
    jointMax.resize(numJoints);

    link = robotModel.getLink(tipName);
    unsigned int i = 0;
    while (link && i < numJoints)
    {
        joint = robotModel.getJoint(link->parent_joint->name);

        if (joint->type != urdf::Joint::UNKNOWN && joint->type != urdf::Joint::FIXED)
        {

            float lower;
            float upper;
            int hasLimits;
            if ( joint->type != urdf::Joint::CONTINUOUS )
            {
                lower = joint->limits->lower;
                upper = joint->limits->upper;
                hasLimits = 1;
            }
            else
            {
                lower = -std::numeric_limits<double>::infinity();
                upper =  std::numeric_limits<double>::infinity();
                hasLimits = 0;
            }
            int index = numJoints - i -1;

            jointMin.data[index] = lower;
            jointMax.data[index] = upper;

            ROS_DEBUG( "getting bounds for joint: [%s], min [%3.4f], max [%3.4f]", joint->name.c_str(), lower, upper );
            i++;
        }
        else
            ROS_DEBUG( "skipping getting bounds for joint: [%s], unknown type or fixed", joint->name.c_str() );


        link = robotModel.getLink(link->getParent()->name);
    }

}




void getJointLimits( const urdf::ModelInterface & robotModel,
                     JointNameToAngleMap &          jointMins,
                     JointNameToAngleMap &          jointMaxs  )
{
    boost::shared_ptr<const urdf::Link>  root = robotModel.getRoot();
    getJointLimits( root, jointMins, jointMaxs );
}


void getJointLimits( const boost::shared_ptr<const urdf::Link> & root,
                     JointNameToAngleMap &          jointMins,
                     JointNameToAngleMap &          jointMaxs  )
{
    typedef boost::shared_ptr<urdf::Joint>       JointPtr;
    typedef boost::shared_ptr<const urdf::Joint> JointConstPtr;
    typedef boost::shared_ptr<urdf::Link>        LinkPtr;
    typedef boost::shared_ptr<const urdf::Link>  LinkConstPtr;
    typedef std::vector<LinkPtr >                LinkPtrVector;

    JointPtr      joint;
    LinkPtrVector children = root->child_links;

    BOOST_FOREACH( LinkPtr child, children )
    {
        joint = child->parent_joint;

        if (joint->type != urdf::Joint::UNKNOWN && joint->type != urdf::Joint::FIXED)
        {
            if ( joint->type != urdf::Joint::CONTINUOUS  && joint->limits )
            {
                jointMins[joint->name] = joint->limits->lower;
                jointMaxs[joint->name] = joint->limits->upper;
            }
            else // continuous no limits
            {
                jointMins[joint->name] = -std::numeric_limits<double>::infinity();
                jointMaxs[joint->name] =  std::numeric_limits<double>::infinity();
            }
            ROS_DEBUG( "getting bounds for joint: [%s], min [%3.4f], max [%3.4f]", joint->name.c_str(), jointMins[joint->name], jointMaxs[joint->name] );
        }
        else
            ROS_DEBUG( "skipping getting bounds for joint: [%s], unknown type or fixed", joint->name.c_str() );

        getJointLimits( child, jointMins, jointMaxs );
    }
}


}
}
